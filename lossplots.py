import numpy as np
import matplotlib.pyplot as plt




minf=264
maxf=277


losses=[]
tlosses=[]
vlosses=[]
comps=[]

for i,fold in enumerate(range(minf,maxf+1)):
  comps.append(i)
  f=np.load(str(fold)+"/data/loss.npz")
  losses.append(f["l"])
  f=np.load(str(fold)+"/data/min_loss.npz")
  tlosses.append(f["q"])
  f=np.load(str(fold)+"/data/min_val_loss.npz")
  vlosses.append(f["q"])

  #f2=np.load(str(fold)+"/roc.npz")
  #aucs.append(f2["auc"])



plt.plot(comps,losses,label="loss")
plt.plot(comps,tlosses,label="train_loss")
plt.plot(comps,vlosses,label="val_loss")

plt.axvline(9,label="other design",color="grey")
plt.axvline(7,label="choosen",color="darkgreen")


plt.xlabel("compression size")
plt.ylabel("loss")

plt.yscale("log",nonposy="clip")

plt.legend()

plt.show()

