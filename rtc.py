import numpy as np
import matplotlib.pyplot as plt
import csv


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def advreadloss(q):
  with open(str(q)+"history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        try:
          q[i].append(float(row[i]))
        except:
          q[i].append(1000000.0)

  for i in range(1,len(q)):
    if qn[i]=="val_loss":return np.min(q[i])
  return 0.0




def read3p(q):

  ret={}
  try:
    ret[0]=(np.load(f"{q}/roc.npz")["auc"])
  except:
    #print("errora")
    pass
  for i in range(1,101):
    try:
      ret[i]=(np.load(str(q)+f'/multi/{i}/roc.npz')["auc"])
    except:
      pass
  return ret



def readl3p(q):

  ret={}
  try:
    if tmode:
      ret[0]=(np.load(str(q)+"/data/loss.npz")["q"])
    else:
      ret[0]=(advreadloss(f"{q}/"))
  except:
    #print("errorb")
    pass
  for i in range(1,101):
    try:
      if tmode:
        ret[i]=(np.load(str(q)+f'/multi/{i}/loss.npz')["q"])
      else:
        ret[i]=(advreadloss(f"{q}/multi/{i}/"))
    except:
      pass
  return ret

def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f

def filterextreme(x,y,maxx=5):
  rx,ry=[],[]
  for ax,ay in zip(x,y):
    if ax>maxx:continue
    rx.append(ax)
    ry.append(ay)
  return rx,ry

def similars(x,y):
  rx,ry=[],[]
  #print(x.keys())
  #print(y.keys())
  #exit()
  for key in x.keys():
    if key in y.keys():
      rx.append(x[key])
      ry.append(y[key])
  return rx,ry
def plotone3p(q,nam,extreme=100):
  y=read3p(q)
  x=readl3p(q)
  x,y=similars(x,y)
  ol=len(x)
  x,y=filterextreme(x,y,maxx=extreme)
  ll=plt.plot(x,y,"o",label=f"{nam}*{len(x)}/{ol}")[0]
  if extreme<1 and legend:plt.plot(x,y,color=ll.get_color(),alpha=0.1)
  xx=np.array(x)[~np.isnan(x)]
  yy=np.array(y)[~np.isnan(y)]
  if len(x)>0:print(f"{len(x)}/{ol}  {nam} [{np.min(xx):.4}-{np.max(xx):.4}]*[{np.min(yy):.4}-{np.max(yy):.4}]")
  if len(x)==0:print(f"{len(x)}/{ol}  {nam}")
  return ol

import sys

moduli=[]
tmode=True#use semitestloss or use validation loss
quiet=False
dispm=False
extrema=0.2
legend=True
logged=False

def isint(q):
  try:
    p=int(q)
    return True
  except:
    return False

for arg in sys.argv:
  if isint(arg):
    moduli.append(int(arg))
  if arg=="vl":
    tmode=False
    print("Entering validation loss mode")
  if arg=="stfu":
    quiet=True
    print("Entering quiet mode")
  if arg=="disp":
    dispm=True
    print("Entering disparity mode")
  if arg=="nl":
    legend=False
    print("Entering no legends mode")
  if arg=="log":
    logged=True
    print("Entering logged mode")

dic={}


if not (0 in moduli):#standart comparison if not 0 added
  dic[456]="higher lr"
if -1 in moduli:#"lower" lr
  dic[369]="standart"

if 1 in moduli:#complex decoder
  acd={
       457:"graphlike",
       458:"paramlike",
      }
  dic.update(acd)

if 2 in moduli:#batch size
  acd={
       459:"500 batch",
       460:"1000 batch",
       461:"50 batch",
       462:"20 batch",
      }
  dic.update(acd)

if 3 in moduli:#norm
  acd={
       463:"l1",
       464:"l3",
       465:"l4",
      }
  dic.update(acd)

if 4 in moduli:#activation
  acd={
       466:"linear",
       467:"softmax",
      }
  dic.update(acd)

if 5 in moduli:#some weird stuff 1
  acd={
       468:"biases",
       469:"redense",
       470:"wide init",
      }
  dic.update(acd)

if 6 in moduli:#training size
  acd={
       471:"data 4",
       472:"data 2",
      }
  dic.update(acd)

if 7 in moduli:#training length
  acd={
       473:"len 100",
       474:"len 500",
       475:"len 1k",
      }
  dic.update(acd)

if 8 in moduli:#training patience
  acd={
       476:"patience 1",
       477:"patience 10",
       478:"patience 100",
       479:"patience 250",
      }
  dic.update(acd)

if 9 in moduli:#combine 1
  dic[480]="c1"

if 10 in moduli:#paramlike graph actions
  acd={
       458:"paramlike",
       481:"paramlike rounded",
       482:"paramlike softmax",
      }
  dic.update(acd)

if 100 in moduli:
  dic[481]="paramlike rounded"

if 11 in moduli:#normal graph actions
  acd={
       483:"rounded",
       484:"softmax",
      }
  dic.update(acd)

if 12 in moduli:#multi layer redense
  acd={
       469:"redense",
       485:"redense 1",
       486:"redense 2",
       487:"redense 4",
       488:"redense 5",
       489:"redense 6",
       490:"redense linear",
      }
  dic.update(acd)

if 13 in moduli:#complexity is good?
  acd={
       491:"2*graph update",
       492:"weird dense",
       493:"usei=True",
      }
  dic.update(acd)

if 14 in moduli:#k=2
  dic[494]="k=2"

if 15 in moduli:#combine 2
  acd={
       495:"c21",
       496:"c22",
       497:"c23",
       502:"paramlike c23",
       509:"train smaller 1% c21",
      }
  dic.update(acd)

if 16 in moduli:
  acd={
       434:"base 16",
       498:"no pt norm",
       499:"pt exp",
       500:"no norm weigthed",
       501:"exp weigthed",
       503:"k=8",
       504:"k=8 paramlike",
       505:"k=12",
       506:"k=16",
       507:"exp weig linear",
       508:"exp weig paramlike",
      }
  dic.update(acd) 
  extrema=50

if 17 in moduli:
  acd={
       509:"loss max 1%",
      }
  dic.update(acd)

if 18 in moduli:
  acd={
       510:"16*k=8,root exp,new standart",
       511:"ml 16*k=8,root exp",
       #512:"ml 16*k=8",
       513:"lr*3.333",
       514:"batch*5",
       515:"less data",
       516:"more data"
      }
  dic.update(acd)
  extrema=50

if 19 in moduli:#linear 4
  acd={
       517:"linear 4"
      }
  dic.update(acd)

if 20 in moduli:#linear 16
  acd={
       523:"linear 16"
      }
  dic.update(acd)
  extrema=50

if 21 in moduli:#power 3 on 4
  acd={
       518:"cubic 4"
      }
  dic.update(acd)

if 22 in moduli:#sqrt(plus) on 4
  acd={
       519:"just root 4",
       520:"root+ 4",
       526:"root+ 4 ldm",
       527:"root+ 4 lqcd",
       535:"root+ 4 nonorm",
       537:"root+ 4 ldm nonorm",
       538:"root+ 4 lqcd nonorm",       
      }
  dic.update(acd)

if 23 in moduli:#sqrt plus on 16
  acd={
       524:"root+ 16",
       532:"root+ 16 ldm",
       533:"root+ 16 lqcd",
       536:"root+ 16 nonorm",
       539:"root+ 16 ldm nonorm",
       540:"root+ 16 lqcd nonorm",
      }
  dic.update(acd)
  extrema=50

if 24 in moduli:#power 2 on 4
  acd={
       521:"quadratic 4 qcd",
       522:"quadratic 4 top",
       528:"quadratic 4 ldm",
       529:"quadratic 4 lqcd",
      }
  dic.update(acd)

if 25 in moduli:#power 2 on 16
  acd={
       525:"quadratic 16",
       #530:"quadratic 16 ldm",
       #531:"quadratic 16 lqcd",
      }
  dic.update(acd)
  extrema=50

if 26 in moduli:#new norming
  acd={
       534:"new norming",
       542:"new norming qcd",
      }
  dic.update(acd)

if -5 in moduli:#story of norm
  acd={
       #495:"c21",
       521:"quadratic 4 qcd",
       522:"quadratic 4 top",
       520:"root+ 4",
       517:"linear 4",
       518:"cubic 4",
       
      }
  dic.update(acd)

if -6 in moduli:#some multi oneoffs
  acd={
       547:"16 top",
       548:"16 qcd",
       549:"16 *10 top",
       550:"16 *10 qcd",
       551:"16 to 0.05% top",
       552:"16 to 0.05% qcd",
       553:"4 to 0.01% top",
       554:"4 to 0.01% qcd",
      }
  dic.update(acd)

if -7 in moduli:
  acd={
       557:"4 qcd mspec",
       558:"4 top mspec"
      }
  dic.update(acd)

if -8 in moduli:#story of norm
  acd={
       #495:"c21",
       521:"quadratic 4 qcd",
       522:"quadratic 4 top",
       
      }
  dic.update(acd)

if -9 in moduli:#story of norm
  acd={
       #495:"c21",
       522:"quadratic 4",
       520:"root+ 4",
       517:"linear 4",
       518:"cubic 4",
       
      }
  dic.update(acd)


def dispone(q,nam):
  global tmode
  tmode=False
  x=readl3p(q)
  tmode=~tmode
  y=readl3p(q)
  x,y=similars(x,y)
  plt.plot(x,y,"o",label=nam)
  if tmode:
    plt.xlabel("validation")
    plt.ylabel("test")
  else:
    plt.ylabel("validation")
    plt.xlabel("test")


if dispm:
  for key in dic.keys():
    dispone(key,dic[key])
    print("read",dic[key])
  plt.yscale("log",nonposy="clip")
  plt.xscale("log",nonposx="clip")


  plt.autoscale(False)
  x=np.arange(-10,10)
  x=np.exp(x)
  plt.plot(x,x,label="identity",color="black",alpha=0.5)


  plt.legend()


  plt.show()
  exit()


  #dic={456:"higher lr",475:"len 1k",479:"patience 250"}
nt=0
for key in dic.keys():
  nt+=plotone3p(key,dic[key],extreme=extrema)
print(f"using {nt} models")

plt.axhline(0.5,color="grey",alpha=0.5)

if legend:plt.legend()

if logged:plt.xscale("log",nonposx="clip")

plt.xlabel("loss")
plt.ylabel("auc")

key=str(sys.argv[1:]).replace("[","").replace("]","").replace("'","").replace(",","").replace("stfu","")

plt.savefig("imgs/rt"+key+".png",format="png")
plt.savefig("imgs/rt"+key+".pdf",format="pdf")


if not quiet:plt.show()

