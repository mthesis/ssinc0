import numpy as np
import matplotlib.pyplot as plt
import csv


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def advreadloss(q):
  with open(str(q)+"history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        try:
          q[i].append(float(row[i]))
        except:
          q[i].append(1000000.0)

  for i in range(1,len(q)):
    if qn[i]=="val_loss":return np.min(q[i])
  return 0.0




def read3p(q):

  ret=[]
  try:
    ret.append(np.load(f"{q}/roc.npz")["auc"])
  except:
    print("errora")
    pass
  for i in range(1,1000):
    try:
      ret.append(np.load(str(q)+f'/multi/{i}/roc.npz')["auc"])
    except:
      break
  return ret



def readl3p(q):

  ret=[]
  try:
    if tmode:
      ret.append(np.load(str(q)+"/data/loss.npz")["q"])
    else:
      ret.append(advreadloss(f"{q}/"))
  except:
    print("errorb")
    pass
  for i in range(1,1000):
    try:
      if tmode:
        ret.append(np.load(str(q)+f'/multi/{i}/loss.npz')["q"])
      else:
        ret.append(advreadloss(f"{q}/multi/{i}/"))
    except:
      break
  return ret

def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f

def filterextreme(x,y,maxx=5):
  rx,ry=[],[]
  for ax,ay in zip(x,y):
    if ax>maxx:continue
    rx.append(ax)
    ry.append(ay)
  return rx,ry

def plotone3p(q,nam,extreme=100):
  y=read3p(q)
  x=readl3p(q)
  if len(y)>len(x):y=y[:len(x)]
  if len(x)>len(y):x=x[:len(y)]
  ol=len(x)
  x,y=filterextreme(x,y,maxx=extreme)
  ll=plt.plot(x,y,"o",label=f"{nam}*{len(x)}/{ol}")[0]
  if extreme<1:plt.plot(x,y,color=ll.get_color(),alpha=0.1)
  xx=np.array(x)[~np.isnan(x)]
  yy=np.array(y)[~np.isnan(y)]
  if len(x)>0:print(f"{len(x)}/{ol}  {nam} [{np.min(xx):.4}-{np.max(xx):.4}]*[{np.min(yy):.4}-{np.max(yy):.4}]")
  if len(x)==0:print(f"{len(x)}/{ol}  {nam}")
  return ol

mode=0
import sys
if len(sys.argv)>1:
  mode=int(sys.argv[1])# % 10

tmode=True#use semitestloss or use validation loss
if len(sys.argv)>2:
  if "vl" in str(sys.argv[2]):
    tmode=True
if not tmode:print("Using only validation loss!")

quiet=False
if sys.argv[-1]=="stfu":quiet=True



if mode%2==0:plotone3p(369,"top")
if mode%2==0:plotone3p(370,"qcd")
if mode>0 and mode<5:plotone3p(378,"lqcd")
if mode>0 and mode<5:plotone3p(379,"ldm")

if mode==5:plotone3p(443,"qcd no norm")
if mode==5:plotone3p(445,"top no norm")
if mode==7:plotone3p(446,"qcd normal norm")
if mode==7:plotone3p(447,"top normal norm")
if mode==9:
  dic={369:"standart",
       456:"higher lr",
       457:"graphlike",
       458:"paramlike",
       459:"500 batch",
       460:"1000 batch",
       461:"50 batch",
       462:"20 batch",
       463:"l1",
       464:"l3",
       465:"l4",
       466:"linear",
       467:"softmax",
       468:"biases",
       469:"redense",
       470:"wide init",
       471:"data 4",
       472:"data 2",
       473:"len 100",
       474:"len 500",
       475:"len 1k",
       476:"patience 1",
       477:"patience 10",
       478:"patience 100",
       479:"patience 250",
      }
  #dic={456:"higher lr",475:"len 1k",479:"patience 250"}
  nt=0
  for key in dic.keys():
    nt+=plotone3p(key,dic[key],extreme=0.2)
  print(f"trained {nt} models")

plt.axhline(0.5,color="grey",alpha=0.5)

plt.legend()

plt.xlabel("loss")
plt.ylabel("auc")

plt.savefig("totalcomp"+str(mode)+".png",format="png")
plt.savefig("totalcomp"+str(mode)+".pdf",format="pdf")


if not quiet:plt.show()

