import numpy as np
import matplotlib.pyplot as plt




minf=264
maxf=277


aucs=[]
waucs=[]
paucs=[]
comps=[]

for i,fold in enumerate(range(minf,maxf+1)):
  comps.append(i)
  f=np.load(str(fold)+"/roc.npz")
  aucs.append(f["auc"])
  f=np.load(str(fold)+"/data/paucw.npz")
  waucs.append(f["auc"])
  f=np.load(str(fold)+"/data/paucp.npz")
  paucs.append(f["auc"])

  #f2=np.load(str(fold)+"/roc.npz")
  #aucs.append(f2["auc"])



plt.plot(comps,aucs,label="auc")
plt.plot(comps,waucs,label="auc angular")
plt.plot(comps,paucs,label="auc pt")

plt.axvline(9,label="other design",color="grey")
plt.axvline(7,label="choosen",color="darkgreen")


plt.xlabel("compression size")
plt.ylabel("auc")

plt.yscale("log",nonposy="clip")

plt.legend()

plt.show()

